  $(function() {

      var start_url = "http://www.bbc.co.uk/radio/programmes/a-z/by/";
      var end_url = "/current.json";

    $( ".prog" ).autocomplete({
        minLength: 2,
        source: function(request, response) {
           var term = request.term.replace(/\s/g, '');
          $.getJSON(start_url+term+end_url, {
          }, function(data) {
            // data is an array of objects and must be transformed for autocomplete to use
            var array = data.error ? [] : $.map(data.atoz.tleo_titles, function(m) {
              return {
                title: m.title,
                description: m.programme.short_synopsis
              };
            });
            response(array);
          });
        },
        focus: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
        },
        select: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
          // input into text field
          $('.prog').val(ui.item.title);
          return false;
        }
      }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var $a = $("<a></a>");
        $("<span class='a-title'></span>").text(item.title).appendTo($a);
        $("<span class='a-desc'></span>").text(item.description).appendTo($a);
      
        return $("<li></li>").append($a).appendTo(ul);
      };
});
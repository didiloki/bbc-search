<?php
/*
 * Project: BBC Job Test
 * Name: Iweala Ebere
 * Date: 16 Sept 2016.
 * 
 */
	include "class_prog.php";  // import class 
	$programme = new Programme(); 


  //Check if the user submited query
 	if(isset($_GET['submit'])){
 		$programName =  $_GET['programme'];
 		
 		$json = $programme->search($programName);
  		
 		}
	
	include('inc/header.inc.php');
?>

  <?php if($programme->error) :?> 
    <div class="alert alert-danger text-center">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <?php echo $programme->errorInformation; //Only show when error returns true ?>
    </div>
  <?php endif ?>

  <?php	echo $programme->displayForm(); // display form?>

<div class="container">
  <section class="col-xs-12 col-sm-12 col-md-12 col-md-offset-2">
    <article class="search-result row">
      <!--./ Loop through and display results for BBC programmes -->
        <?php foreach ($json as $view): ?>
            <div class="col-xs-7 col-sm-7 col-md-7 excerpet">
                <h3><a href="<?php echo $programme->progURL.$view->programme->pid; ?>"> <?php echo $view->title; ?> </a></h3>
                <p>  <?php echo $view->programme->short_synopsis; ?> </p>
                <p>  <?php echo $view->programme->ownership->service->title; ?> </p>
            </div>
                  <span class="clearfix"></span>
        <?php endforeach; ?>
       <!--./ end loop -->
    </article>

    </section>
  </div>

<? include('inc/footer.inc.php'); ?>
<?php

/*--------------
	Name : Iweala Ebere
	Descripttion : BBC Programme and Radio
-----------------*/


class Programme{

	private $start_url = "http://www.bbc.co.uk/radio/programmes/a-z/by/";
	private $end_url = "/current.json";

	public $progURL = "http://www.bbc.co.uk/programmes/"; 
	//error msgs
	public $error = false;
	public $errorInformation = "No Results Found!";

	function displayForm()
	{
		return ('<div class="container">
					  <div class="row">
					        <div class="col-md-12">
					        <h2>Programme Finder</h2>
					        <form action="" type="GET">
					            <div id="custom-search-input">
					                <div class="input-group col-md-12">
					                    <input type="text" name="programme" class="form-control input-lg prog" placeholder="e.g. Citizen Khan" />
					                    <span class="input-group-btn">
					                        <button class="btn btn-info btn-lg" name="submit" type="submit">
					                            <i class="glyphicon glyphicon-search"></i>
					                        </button>
					                    </span>
					                </div>
					            </div>
					        </form>
					        </div>
					  </div>
					</div>');
	}


	function search($progName)
	{
       //search programme
	     $progName = str_replace(' ', '', $progName);
	     //check if request is empty
	     if(empty($progName)){ $this->error = true; }
	     //include my query to url
	     $final_url = file_get_contents($this->start_url.$progName.$this->end_url);
	     //get json
	     $json = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '',$final_url));

	     $programList = $json->atoz->tleo_titles;
		 //check if reponses is empty
		 if(empty($programList)){ $this->error = true; }

		return $programList;
	 }
}

?>
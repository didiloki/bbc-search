<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>BBC Programme</title>

  <meta name="description" content="BBC Search JSON">
  <meta name="author" content="BBC JSON Search">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


  <link rel="stylesheet" href="css/styles.css">

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
	<div class="navbar navbar-inverse">
    	<div class="container">
			<img src="http://www.4over4.com/blog/wp-content/uploads/2014/05/BBC-Logo.jpg" width="120" />
		</div>
	</div>
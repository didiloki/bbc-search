<?php 
  //important variables
  $start_url = "http://www.bbc.co.uk/radio/programmes/a-z/by/";
  $end_url = "/current.json";
  $error = false;
  //error msgs
  $error_info = '<div class="alert alert-danger">';
  $error_info .= 'No Results Found!</div>';
  

  if(isset($_GET['submit'])){
       //search programme
    $programme_name = str_replace(' ', '', $_GET['programme']);

    //check if request is empty
    if(empty($programme_name)){
        $error = true;
        }
    //include my query to url
    $final_url = file_get_contents($start_url.$programme_name.$end_url);
    
    //get json
    $json = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '',$final_url));

    //check if reponses is empty
    if(empty($json->atoz->tleo_titles)){
        $error = true;
    }
  }
  
?>
<?php include('inc/header.inc.php'); //include header ?>

<?php include('form.php'); // form for search ?>
  
<div class="container">
  <section class="col-xs-12 col-sm-6 col-md-12">
    <article class="search-result row">
      <!--./ Loop through and display results for BBC programmes -->
        <?php foreach ($json->atoz->tleo_titles as $view): ?>
            <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                <h3> <?php echo $view->title; ?> </h3>
                <p>  <?php echo $view->programme->short_synopsis; ?> </p>
                <p>  <?php echo $view->programme->ownership->service->title; ?> </p>
            </div>
        <?php endforeach; ?>
       <!--./ end loop -->

      <span class="clearfix"></span>
    </article>

    </section>
<?php include('inc/footer.inc.php'); ?>
<div class="container">
  <div class="row">
        <div class="col-md-6">
        <h2>Programme Finder</h2>
        <?php if($error){ echo $error_info; } ?>
        <form action="" type="GET">
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" name="programme" class="form-control input-lg prog" placeholder="e.g. Citizen Khan" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" name="submit" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
        </div>
  </div>
</div>